
public class ArrayReverse {
	public static void main(String[] args) {
		int arr[] = new int[] { 10, 30, 25, 40, 85, 97, 20, 5 };
		System.out.println("Before reversing the elements:");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		System.out.println("After reversing the elements:");
		for (int i = arr.length - 1; i >= 0; i--) {
			System.out.println(arr[i]);
		}
	}

}
